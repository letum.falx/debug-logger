const debug = require('debug');

/**
 * The list of default configs value.
 */
const defaultConfigs = {
  channels: {
    /**
     * Logging channel for "debug" level.
     */
    debug: console.debug.bind(console),
    /**
     * Logging channel for "error" level.
     */
    error: console.error.bind(console),
    /**
     * Logging channel for "info" level.
     */
    info: console.info.bind(console),
    /**
     * Logging channel for "log" level.
     */
    log: console.log.bind(console),
    /**
     * Logging channel for "warn" level.
     */
    warn: console.warn.bind(console)
  },

  /**
   * The namespace to be prepended to the given namespace.
   *
   * @type {Stirng}
   */
  baseNamespace: '',

  /**
   * The separator to be used for concatenating the namespaces.
   *
   * @type {String}
   */
  separator: ':'
};

/**
 * The configuration for the logger.
 */
class LoggerConfig {
  /**
   * The list of defaults configs.
   */
  static get defaults () {
    return defaultConfigs;
  }

  /**
   * Create an instance of logger config.
   * @param {LoggerConfig} config
   */
  constructor (config = {}) {
    const {
      channels: {
        debug,
        error,
        info,
        log,
        warn
      } = {},
      baseNamespace,
      separator
    } = config;

    const {
      channels: defaultChannels,
      baseNamespace: defaultBaseNamespace,
      separator: defaultSeparator
    } = LoggerConfig.defaults;

    /**
     * The list of channels where to log.
     */
    this.channels = {
      /**
       * Logging channel for "debug" level.
       */
      debug: debug || defaultChannels.debug,
      /**
       * Logging channel for "error" level.
       */
      error: error || defaultChannels.error,
      /**
       * Logging channel for "info" level.
       */
      info: info || defaultChannels.info,
      /**
       * Logging channel for "log" level.
       */
      log: log || defaultChannels.log,
      /**
       * Logging channel for "warn" level.
       */
      warn: warn || defaultChannels.warn
    };

    /**
     * The namespace to be prepended to the given namespace.
     *
     * @type {Stirng}
     */
    this.baseNamespace = baseNamespace || defaultBaseNamespace;

    /**
     * The separator to be used for concatenating the namespaces.
     *
     * @type {String}
     */
    this.separator = separator || defaultSeparator;
  }
}

/**
 * The wrapper for visionmedia/debug that maps the console output channels.
 */
class Logger {
  /**
   * The configuration object for the logger.
   */
  static get LoggerConfig () {
    return LoggerConfig;
  }

  /**
   * Get the original debug function.
   */
  static get originalDebug () {
    return debug;
  }

  /**
   * Create an instance of Logger.
   *
   * @param {String|String[]} namespace
   * @param {LoggerConfig} configs
   */
  constructor (namespace, configs = {}) {
    /**
     * The configs for this logger. Updating this has no effect on existing logging
     * channel of this logger.
     *
     * @type {LoggerConfig}
     */
    this.configs = configs instanceof LoggerConfig ? configs : new LoggerConfig(configs);

    const {
      separator,
      baseNamespace,
      channels
    } = this.configs;

    namespace = Array.isArray(namespace) ? namespace.join(separator) : namespace;

    /**
     * The full namespace of this logger.
     *
     * @type {String}
     */
    this.namespace = `${baseNamespace}${baseNamespace ? separator : ''}${namespace}`;

    const createDebugInstance = level => {
      const _debug = debug([this.namespace, level].join(separator));
      _debug.log = channels[level];
      _debug.enabled = true;
      return _debug;
    };

    /**
     * The "debug" level logger.
     */
    this.debug = createDebugInstance('debug');

    /**
     * The "debug" level logger.
     */
    this.error = createDebugInstance('error');

    /**
     * The "info" level logger.
     */
    this.info = createDebugInstance('info');

    /**
     * The "log" level logger.
     */
    this.log = createDebugInstance('log');

    /**
     * The warn level logger.
     */
    this.warn = createDebugInstance('warn');
  }

  /**
   * Creates a new logger from this logger.
   *
   * @param {String} namespace the namespace to add to existing namespace
   * @param {LoggerConfig} configs the configs to override
   * @return {Logger}
   */
  extend (namespace, configs = {}) {
    const { baseNamespace, ...overrides } = configs;
    return new Logger(namespace, {
      ...overrides,
      baseNamespace: this.namespace
    });
  }
}

module.exports = Logger;
