// import the package
var Logger = require('../Logger');

// create an instance
var logger = new Logger('namespace', {
  separator: '-',
  baseNamespace: 'base'
});

// log
logger.debug('logs on "debug" channel');
logger.error('logs on "error" channel');
logger.info('logs on "info" channel');
logger.log('logs on "log" channel');
logger.warn('logs on "warn" channel');
