var Logger = require('../Logger');
var logger = new Logger(['base', 'namespace']);

var extendedLogger = logger.extend('extend', {
  separator: '-'
});

// log
extendedLogger.debug('logs on "debug" channel');
extendedLogger.error('logs on "error" channel');
extendedLogger.info('logs on "info" channel');
extendedLogger.log('logs on "log" channel');
extendedLogger.warn('logs on "warn" channel');
