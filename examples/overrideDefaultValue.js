var Logger = require('../Logger');

var original = new Logger('namespace');
original.info('Log "original"');

// override the baseNamespace
Logger.LoggerConfig.defaults.baseNamespace = 'override';

var logger = new Logger('namespace');
logger.info('Log "overriden"');
