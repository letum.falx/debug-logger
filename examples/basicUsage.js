const Logger = require('../Logger');

const logger = new Logger('namespace');

// log
logger.debug('logs on "debug" channel');
logger.error('logs on "error" channel');
logger.info('logs on "info" channel');
logger.log('logs on "log" channel');
logger.warn('logs on "warn" channel');
