# Debug Logger

A wrapper for [visionmedia/debug](https://www.npmjs.com/package/debug) package. This maps the package to the five `console` debug level:

- `debug`
- `error`
- `info`
- `log`
- `warn`

## Installation

```
npm install @letumfalx/debug-logger
```

## Usage

### Basic Usage

```
// import the package
var Logger = require('@letumfalx/debug-logger');

// create an instance
var logger = new Logger('namespace');

// log
logger.debug('logs on "debug" channel');
logger.error('logs on "error" channel');
logger.info('logs on "info" channel');
logger.log('logs on "log" channel');
logger.warn('logs on "warn" channel');
```

Above will output:

```
  namespace:debug logs on "debug" channel +0ms
  namespace:error logs on "error" channel +0ms
  namespace:info logs on "info" channel +0ms
  namespace:log logs on "log" channel +0ms
  namespace:warn logs on "warn" channel +0ms
```

### Passing Configs

```
// import the package
var Logger = require('@letumfalx/debug-logger');

// create an instance
var logger = new Logger('namespace', {
  separator: '-',
  baseNamespace: 'base'
});

// log
logger.debug('logs on "debug" channel');
logger.error('logs on "error" channel');
logger.info('logs on "info" channel');
logger.log('logs on "log" channel');
logger.warn('logs on "warn" channel');
```

Above will output:

```
  base-namespace-debug logs on "debug" channel +0ms
  base-namespace-error logs on "error" channel +0ms
  base-namespace-info logs on "info" channel +0ms
  base-namespace-log logs on "log" channel +0ms
  base-namespace-warn logs on "warn" channel +0ms
```

### Extending the Instance

You can extend the logger, appending the namespace to the namespace of the base logger.

```
var Logger = require('@letumfalx/debug-logger');
var logger = new Logger('namespace');
var extendedLogger = logger.extend('extend');

// log
extendedLogger.debug('logs on "debug" channel');
extendedLogger.error('logs on "error" channel');
extendedLogger.info('logs on "info" channel');
extendedLogger.log('logs on "log" channel');
extendedLogger.warn('logs on "warn" channel');
```

Above will output:

```
  namespace:extend:debug logs on "debug" channel +0ms
  namespace:extend:error logs on "error" channel +0ms
  namespace:extend:info logs on "info" channel +0ms
  namespace:extend:log logs on "log" channel +0ms
  namespace:extend:warn logs on "warn" channel +0ms
```

### Overriding Config on Extend

You can extend the logger, appending the namespace to the namespace of the base logger.

```
var Logger = require('@letumfalx/debug-logger');
var logger = new Logger(['base', 'namespace']);
var extendedLogger = logger.extend('extend', {
  separator: '-'
});

// log
extendedLogger.debug('logs on "debug" channel');
extendedLogger.error('logs on "error" channel');
extendedLogger.info('logs on "info" channel');
extendedLogger.log('logs on "log" channel');
extendedLogger.warn('logs on "warn" channel');
```

Above will output:

```
  base:namespace-extend-debug logs on "debug" channel +0ms
  base:namespace-extend-error logs on "error" channel +0ms
  base:namespace-extend-info logs on "info" channel +0ms
  base:namespace-extend-log logs on "log" channel +0ms
  base:namespace-extend-warn logs on "warn" channel +0ms
```

**Notes:**

- `baseNamespace` cannot be overridden, it will always be the current namespace of the base namespace.
- `separator` will only affect the new appending namespaces.

## Configurations

This are the configuration to be used for creating logger.

```
{
  channels: {
    debug: Function,
    error: Function,
    info: Function,
    log: Function,
    warn: Function
  },
  baseNamespace: String,
  separator: String
}
```

### channels

This are the functions to be called to log messages. By default this will be the console channels ( ex: `debug: console.debug.bind(console)` ).

### baseNamespace

The namespace to be prepended to the passed namespace.

**Example:**

```
namespace: 'namespace'
baseNamespace: 'base'
result: 'base:namespace'
```

### separator

The separator to be used for appending the namespaces.

### Default Values

```
{
  channels: {
    debug: console.debug.bind(console),
    error: console.error.bind(console),
    info: console.error.bind(console),
    log: console.log.bind(console),
    warn: console.warn.bind(console)
  },
  baseNamespace: '',
  separator: ':'
}
```

### Overriding Default Value

You can override any of the default value.

```
var Logger = require('@letumfalx/debug-logger');

var original = new Logger('namespace');
original.info('Log "original"');

// override the baseNamespace
Logger.LoggerConfig.defaults.baseNamespace = 'override';

var logger = new Logger('namespace');
logger.info('Log "overriden"');
```

Above code will output:

```
  namespace:info Log "original" +0ms
  override:namespace:info Log "overriden" +0ms
```
